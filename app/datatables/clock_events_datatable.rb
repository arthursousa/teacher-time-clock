class ClockEventsDatatable < AjaxDatatablesRails::Base
  def_delegators :@view, :link_to, :edit_clock_event_path, :clock_event_path

  def view_columns
    @view_columns ||= {
      datetime:    { source: "ClockEvent.id" },
      type:        { source: "ClockEvent.event_type_id" },
      links:       { source: "ClockEvent.id" }
    }
  end

  def data
    records.map do |record|
      {
        datetime:     record.created_at.strftime("%d/%m/%Y - %I:%M:%S %p"),
        type:         record.event_type.name.upcase,
        links:        link_to("edit", edit_clock_event_path(record), class:"grey-link") + " / " + link_to("delete", clock_event_path(record), method: :delete, class:"red-link")
      }
    end
  end

  def get_raw_records
    ClockEvent.where(teacher_id: options[:current_teacher].id)
  end

end