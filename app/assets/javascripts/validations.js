const PASSWORD_MIN_LENGTH = 6;

$(document).on('turbolinks:load', function() {
  $("#name-field").blur(isTextFieldFilled);
  $("#email-field").blur(isEmailValid);
  $("#password-field").blur(isPasswordValid);

  $("#datetime-field").blur(isTextFieldFilled);

  // Submit events
  $("#submit-data").click(validateSubmissionData);
})

function isTextFieldFilled(element) {
  if (element instanceof jQuery.Event) element = $(this);

  // return true when element is not present
  if (element.val() == null) {
    return true;
  }

  if (element.val() == "") {
		showError(element, "Required field");
		return false;
  }

  resetField(element);
	return true;
}

function isValidEmailFormat(email) {
  regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return regex.test(email.val())
}

function isEmailValid(email) {
  if(email instanceof jQuery.Event) email = $(this);

  // return true when email is not present
  if (email.val() == null) {
    return true;
  }

  if (!isTextFieldFilled(email)) return false;

  if (!isValidEmailFormat(email)) {
    showError(email, "Invalid Email");
    return false;
  }

  resetField(email);
  return true;
}

function isPasswordValid(password) {
  if(password instanceof jQuery.Event) password = $(this);

  // return true when password is not present
  if (password.val() == null) {
    return true;
  }

  if (!isTextFieldFilled(password)) return false;

  if (password.val().length < PASSWORD_MIN_LENGTH) {
    showError(password, "Password needs to have at least " + PASSWORD_MIN_LENGTH + " characters");
    return false;
  }

  resetField(password);
  return true;
}

function validateSubmissionData() {
  if (!isTextFieldFilled($("#name-field"))) {
    return false;
  }

  if (!isEmailValid($("#email-field"))) {
    return false;
  }

  if (!isPasswordValid($("#password-field"))) {
    return false;
  }

  if (!isTextFieldFilled($("#datetime-field"))) {
    return false;
  }

  return true;
}

function showError(element, message) {
  element.parents(".form-field").css('margin-bottom', '20px');
	element.addClass("invalid");

	if (element.parents(".form-field").children(".form-error-warning").length === 0) {
		errorElement = "<div class='form-error-warning'><i class='material-icons'>warning</i><span>" + message + "</span></div>";
		element.parents(".form-field").append(errorElement).children(".form-error-warning").fadeIn();
		return;
	}

	element.parents(".form-field").children(".form-error-warning").children("span").text(message);
}

function resetField(element) {
  element.parents(".form-field").css('margin-bottom', 'auto');
	element.removeClass("invalid").parents(".form-field").children(".form-error-warning").remove();
}