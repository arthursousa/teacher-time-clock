$(document).on('turbolinks:load', function() {
  $("#clock-events-datatable").DataTable({
    "serverSide": true,
    "searching": false,
    "bLengthChange": false,
    "order": [[ 0, "desc" ]],
    "ajax": $("#clock-events-datatable").data("source"),
    "columns": [
      {"data": "datetime"},
      {"data": "type"},
      {"data": "links"}
    ]
  });
});