class ClockEventsController < ApplicationController
  before_action :validate_teacher
  before_action :set_clock_event, except:[:create] 

  def create
    # Check if there are any clock events and sets the new event type to the opposite of the last clock event type
    new_event_type = current_teacher.clock_events.empty? || current_teacher.clock_events.last.event_type.out? ? EventType.in : EventType.out

    clock_event = ClockEvent.new(event_type_id: new_event_type.first.id, teacher_id: current_teacher.id)
    clock_event.save

    redirect_to teacher_time_clock_path(current_teacher)
  end

  def edit
  end
  
  def update
    if @clock_event.update(clock_event_update_params)
      flash[:success] = "Time Clock Event updated"
      redirect_to teacher_time_clock_path(current_teacher)
    else
      flash.now[:error] = @clock_event.errors.full_messages.first
      render 'edit'
    end
  end

  def destroy
    if @clock_event.destroy
      flash[:success] = "Time Clock Event deleted"
    else
      flash.now[:error] = @clock_event.errors.full_messages.first
    end
    
    redirect_to teacher_time_clock_path(current_teacher)
  end

  private

  def set_clock_event
    @clock_event = ClockEvent.find(params[:id])
  end

  def clock_event_update_params
    params.require(:clock_event).permit(:created_at)
  end

end
