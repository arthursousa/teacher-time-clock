class TeachersController < ApplicationController
  before_action :validate_teacher

  def time_clock
    respond_to do |format|
      format.html
      format.json { render json: ClockEventsDatatable.new(view_context, { current_teacher: current_teacher }) }
    end
  end

end
