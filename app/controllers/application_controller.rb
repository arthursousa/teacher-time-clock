class ApplicationController < ActionController::Base

  def after_sign_in_path_for(resource)
    current_teacher ? teacher_time_clock_path(current_teacher) : root_path
  end

  def validate_teacher
    unless teacher_signed_in?
      flash.now[:error] = "Sign in to continue"
      redirect_to root_path unless teacher_signed_in?
    end
  end
  
end
