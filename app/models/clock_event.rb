class ClockEvent < ApplicationRecord
  belongs_to :event_type
  belongs_to :teacher

  validate :created_at_validation, on: :update

  private

  # New created_at value will only be valid if:
  # - It has the right datetime format
  # - It's not before or equal the clock event that happened before it  (in case it exists)
  # - It's not equal or after the clock event that happened after it (in case it exists)
  # - It's not after Time now in EDT
  def created_at_validation
    if self.created_at.nil?
      message = "Invalid datetime format. Expected: dd/mm/yyyy - hh:mm:ss AM/PM."
      errors.add(:base, message)
      return
    end

    new_created_time = self.created_at.strftime("%d/%m/%Y - %I:%M:%S %p")

    previous_event = teacher.clock_events.where("id < ?", self.id).last
    if previous_event.present? && new_created_time <= previous_event.created_at.strftime("%d/%m/%Y - %I:%M:%S %p")
      message = "You clocked " + previous_event.event_type.name + " previously at " + previous_event.created_at.strftime("%d/%m/%Y - %I:%M:%S %p") + "."
      message += " Please choose a different clock " + self.event_type.name + " time."

      errors.add(:base, message)
      return
    end

    next_event = teacher.clock_events.where("id > ?", self.id).first
    if next_event.present? && new_created_time >= next_event.created_at.strftime("%d/%m/%Y - %I:%M:%S %p")
      message = "You already clocked " + next_event.event_type.name + " at " + next_event.created_at.strftime("%d/%m/%Y - %I:%M:%S %p") + "."
      message += " Please choose a different clock " + self.event_type.name + " time."

      errors.add(:base, message)
      return
    end

    time_now = Time.now.in_time_zone("Eastern Time (US & Canada)").strftime("%d/%m/%Y - %I:%M:%S %p")
    if new_created_time > time_now
      message = "Clock event cannot be updated to a time in the future."
      errors.add(:base, message)
      return
    end
  end

end