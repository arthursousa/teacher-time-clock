class EventType < ApplicationRecord
  enum name: { in: 1, out: 2 }

  # There is no point in keeping the clock_events if the event types are deleted
  has_many :clock_events, dependent: :destroy
  
  validates :name, presence: true, uniqueness: true
end
