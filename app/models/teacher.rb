class Teacher < ApplicationRecord
  # Include default devise modules. Others available are:
  # :recoverable, :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable, :validatable, :rememberable

  # It only makes sense for clock events to exist when they are associated to a Teacher  
  has_many :clock_events, dependent: :destroy

  validates_presence_of :name

  validates :email, 
            presence: true,
            uniqueness: true

  validates :password,
            length: { minimum: 6 },
            presence: true, on: :create

end
