module ApplicationHelper

  def page_title(page_title)
    content_for :page_title, page_title
  end
  
  def page_description(page_description)
    content_for :meta_description, page_description
  end

  def show_toast_error(message)
    javascript_tag("M.toast({html: '#{message}', displayLength: 8000})") unless message.blank?
  end

end
