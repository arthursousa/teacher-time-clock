# TeacherTimeClock

A platform that keeps track of teachers' clock-in and clock-out times.

Gitlab repo: https://gitlab.com/arthursousa/teacher-time-clock

Working prototype: https://teacher-time-clock.herokuapp.com/

## Discussion

The following technologies were used: HTML, CSS (Materialize), Javascript (jQuery), and other libraries like: devise (Authentication solution) and ajax-datatables (Datatable solution).

## Requirements

##### Using Ruby on Rails (or equivalent MVC framework), build a basic MVP of a clock-in/out application, using traditional CRUD methods. The app should Present a clock-in/out screen that will:

- Allow a user to enter their name or log in

The application's home page gives teachers options to sign up (name, email, password) and sign in (email, password).

- Allow the user to clock either in or out multiple times a day (e.g., for lunch)

After signing in, the teacher is taken to its time clock page in which they can clock in or clock out multiple times.

- Upon clock event, store this information

The clock event information is then stored in the database.

- Provide a list of all clock events with logged information and a clock event may need to be edited or deleted

In the same time clock page, teachers can see a list of all previous clock events and edit/delete previous clock events.

- Are there any validations or UI constraints.

Front-end and back-end validations were created to ensure the constraints would be satisfied.