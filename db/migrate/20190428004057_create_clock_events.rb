class CreateClockEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :clock_events do |t|
      t.timestamps
    end
    add_reference :clock_events, :event_type, index: true
    add_reference :clock_events, :teacher, index: true
  end
end
