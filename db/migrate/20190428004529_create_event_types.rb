class CreateEventTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :event_types do |t|
      t.integer :name, default: 1, null: false
      t.timestamps
    end
  end
end
