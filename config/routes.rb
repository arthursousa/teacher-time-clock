Rails.application.routes.draw do
  root to: "home#show"

  devise_for :teachers, :skip => [:passwords, :confirmations, :omniauth_callbacks, :unlocks], controllers: {
    registrations: 'teachers/registrations',
    sessions: 'teachers/sessions'
  }

  resources :teachers, only: [] do
    get "time-clock" => "teachers#time_clock"
  end

  resources :clock_events, only: [:create, :edit, :update, :destroy]

end