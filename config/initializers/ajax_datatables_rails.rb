AjaxDatatablesRails.configure do |config|
  if Rails.env.development?
    config.db_adapter = :sqlite3
  else
    config.db_adapter = :pg
  end

  config.orm = :active_record
end